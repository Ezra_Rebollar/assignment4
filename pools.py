import os
import time

import requests
from flask import request, Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from xml.etree import ElementTree as ET
import urllib.parse as urlparse
from urllib.parse import parse_qs
import json 
import re


application = Flask(__name__)
app = application
all_pools = []

def get_db_creds():
  db = os.environ.get("DB", None) or os.environ.get("database", None)
  username = os.environ.get("USER", None) or os.environ.get("username", None)
  password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
  hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
  return db, username, password, hostname

def create_table():
  # Check if table exists or not. Create and populate it only if it does not exist.
  db, username, password, hostname = get_db_creds()
  table_ddl = 'CREATE TABLE pools_web (pool_name varchar(100) NOT NULL, status varchar(100) NOT NULL, phone varchar(100) NOT NULL, pool_type varchar(100) NOT NULL,  PRIMARY KEY (pool_name))'
  cnx = ''
  try:
    cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
  except Exception as exp:
    print(exp)
    import MySQLdb
    # try:
    cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
  # except Exception as exp1:
  # print(exp1)
  cur = cnx.cursor()
  try:
    cur.execute(table_ddl)
    cnx.commit()
    #populate_data()
  except mysql.connector.Error as err:
    if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
      print()
    else:
      print(err.msg)

def read_curl_file(file):
  my_file = request.json
  return str(my_file)

@app.route("/static/add_pool", methods=['POST'])
def add_pool():

    # Assignment 4: Insert Pool into database.
    # Extract all the form fields
    in_pool_name = request.form['pool_name']
    in_pool_status = request.form['status']
    in_pool_phone = request.form['phone']
    in_pool_type = request.form['pool_type']
    print()
    print("Pool Name: " + in_pool_name)
    print("Status: " + in_pool_status)
    print("Phone: " + str(in_pool_phone))
    print("Pool Type: " + in_pool_type)
    print()

    # POST a new pool into datbase
    if request.method == "POST":
        # connect to datbase
        db, username, password, hostname = get_db_creds()
        cnx = ''
        try:
            cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
        except Exception as exp:
            print(exp)
            import MySQLdb
            cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # Insert into database.

    cur = cnx.cursor()
    cur.execute("INSERT IGNORE INTO pools_web (pool_name, status, phone, pool_type) values ('"+in_pool_name+"', '"+in_pool_status+"', '"+in_pool_phone+"', '"+in_pool_type+"')")
    cnx.commit()


    return render_template('pool_added.html')


@app.route("/pools")
def get_pools():
    # Assignment 4: 
    # Query the database to pull all the pools
    # Sample pool -- Delete this from final output.
    # get info about specific pool 
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * from pools_web")

    row = cur.fetchall()

    for i in range(len(row)):
        wanted_dict = {}
        wanted_dict["Name"] = row[i][0]
        wanted_dict["Status"] = row[i][1]
        wanted_dict["Phone"] = row[i][2]
        wanted_dict["Type"] = row[i][3]
        all_pools.append(wanted_dict)
    

    return json.dumps(all_pools) 


@app.route("/")
def pool_info_website():
    create_table()
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
